package newphonebook;

public class Contact {
	
	private String name;
	private String number;
	private String emailAdress;
	
	public Contact(String name, String number, String emailAdress) {
		this.name = name;
		this.number = number;
		this.emailAdress = emailAdress;
	}
	
	public String getName() {
		return name;
	}
	
	public String getNumber() {
		return number;
	}
	
	public String getEmailAdress() {
		return emailAdress;
	}
	

}
