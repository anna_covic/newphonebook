package newphonebook;

import java.util.Scanner;

public class App {
	
	private static Scanner input = new Scanner(System.in);
	private static ContactList contactList = new ContactList();

	public static void main(String[] args) {
		
		
		boolean exit = false;
		openPhonebook();
		showMenu();
		while(!exit) {
			System.out.println("Enter the number of the desired action: \n"
					+ "Press 0 to show options.");
			int option = input.nextInt();
			input.nextLine();
			
			switch (option) {
			case 0:
				showMenu();
				break;
			case 1:
				contactList.printPhonebook();
				break;
			case 2:
				addContact();
				break;
			case 3:
				removeExistingContact();
				break;
			case 4:
				System.out.println("Closing phonebook.");
				exit = true;
				break;
				
			}
		}

	}
	
	private static void openPhonebook() {
		System.out.println("PHONEBOOK\n"
				+ "Manage your contacts.");
	}
	
	private static void showMenu() {
		System.out.println("\nWhat would you like to do?\n");
		System.out.println("0 - show the list of available options\n"
				+ "1 - show all contacts in your phonebook\n"
				+ "2 - add a new contact to your phonebook\n"
				+ "3 - remove an existing contact\n"
				+ "4 - exit phonebook");
		System.out.println("Choose from the available options:");
	}
	
	private static void addContact() {
		
		System.out.println("Enter new contact name: ");
		String name = input.nextLine();
		System.out.println("Enter phone number: ");
		String number = input.nextLine();
		System.out.println("Enter e-mail adress: ");
		String email = input.nextLine();
		Contact person = new Contact(name, number, email);
		contactList.addNewContact(person);
		System.out.println("New contact successfully added.");
		
	}
	
	private static void removeExistingContact() {
		System.out.println("Enter the name of the contact you want to remove: ");
		String name = input.nextLine();
		contactList.removeContact(contactList.getContactFromName(name));
	}
	

}
