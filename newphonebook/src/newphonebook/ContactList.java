package newphonebook;

import java.util.ArrayList;

public class ContactList {
	
	private ArrayList<Contact> phonebook = new ArrayList<>();
	
	public void addNewContact(Contact contact) {
		phonebook.add(contact);
	}
	
	public void removeContact(Contact contact) {
		int position = findContactPosition(contact);
		this.phonebook.remove(position);
		System.out.println("Contact successfully deleted!");
	}
	
	private int findContactPosition(Contact contact) {
		return this.phonebook.indexOf(contact);
	}
	
	public void printPhonebook() {
		System.out.println("Phonebook");
		for (int i = 0; i < this.phonebook.size(); i++) {
			System.out.println((i+1) + ".\t"
					+ "Name:\t" + this.phonebook.get(i).getName()
					+ "\tNumber:\t" + this.phonebook.get(i).getNumber()
					+ "\tE-mail:\t" + this.phonebook.get(i).getEmailAdress());
		}
	}
	
	public Contact getContactFromName(String name) {
		Contact person = null;
		for (int i = 0; i < this.phonebook.size(); i++) {
			if (this.phonebook.get(i).getName().equals(name)) {
				 person = this.phonebook.get(i);
			}
			else {
				System.out.println("Contact not found.");
			}
		}
		return person;
	}
}